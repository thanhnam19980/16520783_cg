#include "TriAngle.h"



void Triangle::Process_Triangle() {
	a = sqrt((C.x - B.x)*(C.x - B.x) + (C.y - B.y)*(C.y - B.y));
	b = sqrt((A.x - C.x)*(A.x - C.x) + (A.y - C.y)*(A.y - C.y));
	c = sqrt((A.x - B.x)*(A.x - B.x) + (A.y - B.y)*(A.y - B.y));

	Ang_A = acos(float((b*b + c * c - a * a) / (2 * b*c)));
	Ang_B = acos(float((a*a + c * c - b * b) / (2 * a*c)));
	Ang_C = acos(float((a*a + b * b - c * c) / (2 * b*a)));

	a_AB = B.y - A.y;
	b_AB = A.x - B.x;
	c_AB = -A.x*B.y + B.x*A.y;

	a_BC = C.y - B.y;
	b_BC = B.x - C.x;
	c_BC = -B.x*C.y + C.x*B.y;

	a_AC = C.y - A.y;
	b_AC = A.x - C.x;
	c_AC = -A.x*C.y + C.x*A.y;
}
Triangle::Triangle() {

	a = sqrt((C.x - B.x)*(C.x - B.x) + (C.y - B.y)*(C.y - B.y));
	b = sqrt((A.x - C.x)*(A.x - C.x) + (A.y - C.y)*(A.y - C.y));
	c = sqrt((A.x - B.x)*(A.x - B.x) + (A.y - B.y)*(A.y - B.y));

	Ang_A = acos(float((b*b + c * c - a * a) / (2 * b*c)));
	Ang_B = acos(float((a*a + c * c - b * b) / (2 * a*c)));
	Ang_C = acos(float((a*a + b * b - c * c) / (2 * b*a)));

	a_AB = B.y - A.y;
	b_AB = A.x - B.x;
	c_AB = -A.x*B.y + B.x*A.y;

	a_BC = C.y - B.y;
	b_BC = B.x - C.x;
	c_BC = -B.x*C.y + C.x*B.y;

	a_AC = C.y - A.y;
	b_AC = A.x - C.x;
	c_AC = -A.x*C.y + C.x*A.y;
}

void Triangle::Draw_Triangle(Point A, Point B, Point C, SDL_Renderer *ren) {
	Process_Triangle();
	Bresenham_Line(A.x, A.y, B.x, B.y, ren);
	Bresenham_Line(B.x, B.y, C.x, C.y, ren);
	Bresenham_Line(A.x, A.y, C.x, C.y, ren);
}

void Triangle::Draw_Bisectrix(Point Angle, SDL_Renderer *ren,Point &gd) {
	float a_bis, b_bis, c_bis;
	Point temp;
	if (Angle.Name == "A") {
		float d = sqrt((a_AB*a_AB) + (b_AB * b_AB)) / sqrt((a_AC*a_AC) + (b_AC * b_AC));
		a_bis = ((a_AC * d) - a_AB);
		b_bis = ((b_AC * d) - b_AB);
		c_bis = ((c_AC * d) - c_AB);
		if (((a_bis * B.x) + (b_bis * B.y) + c_bis) * ((a_bis * C.x) + (b_bis * C.y) + c_bis) > 0) {
			a_bis = ((a_AC * d) + a_AB);
			b_bis = ((b_AC * d) + b_AB);
			c_bis = ((c_AC * d) + c_AB);
		}

		temp.x = ((b_bis*c_BC) - (b_BC*c_bis)) / ((a_bis*b_BC) - (a_BC*b_bis));
		temp.y = ((a_BC*c_bis) - (a_bis*c_BC)) / ((a_bis*b_BC) - (a_BC*b_bis));

		Bresenham_Line(A.x, A.y, temp.x, temp.y, ren);
	}
	else if (Angle.Name == "B") {
		float d = sqrt((a_AB * a_AB) + (b_AB * b_AB)) / sqrt((a_BC * a_BC) + (b_BC * b_BC));
		a_bis = ((a_BC * d) - a_AB);
		b_bis = ((b_BC * d) - b_AB);
		c_bis = ((c_BC * d) - c_AB);
		if (((a_bis * A.x) + (b_bis * A.y) + c_bis) * ((a_bis * C.x) + (b_bis * C.y) + c_bis) > 0) {
			a_bis = ((a_BC * d) + a_AB);
			b_bis = ((b_BC * d) + b_AB);
			c_bis = ((c_BC * d) + c_AB);
		}

		temp.x = ((b_bis*c_AC) - (b_AC*c_bis)) / ((a_bis*b_AC) - (a_AC*b_bis));
		temp.y = ((a_AC*c_bis) - (a_bis*c_AC)) / ((a_bis*b_AC) - (a_AC*b_bis));

		Bresenham_Line(B.x, B.y, temp.x, temp.y, ren);
	}
	else {
		float d = sqrt((a_AC * a_AC) + (b_AC * b_AC)) / sqrt((a_BC * a_BC) + (b_BC * b_BC));
		a_bis = ((a_BC * d) - a_AC);
		b_bis = ((b_BC * d) - b_AC);
		c_bis = ((c_BC * d) - c_AC);
		if (((a_bis * A.x) + (b_bis * A.y) + c_bis) * ((a_bis * B.x) + (b_bis * B.y) + c_bis) > 0) {
			a_bis = ((a_BC * d) + a_AC);
			b_bis = ((b_BC * d) + b_AC);
			c_bis = ((c_BC * d) + c_AC);
		}

		temp.x = ((b_bis*c_AB) - (b_AB*c_bis)) / ((a_bis*b_AB) - (a_AB*b_bis));
		temp.y = ((a_AB*c_bis) - (a_bis*c_AB)) / ((a_bis*b_AB) - (a_AB*b_bis));

		Bresenham_Line(C.x, C.y, temp.x, temp.y, ren);
	}
	gd = temp;
}


Point Triangle::Get_Midpoint(Point A, Point B) {
	Point MidPoint;
	MidPoint.x = (A.x + B.x) / 2;
	MidPoint.y = (A.y + B.y) / 2;
	return MidPoint;
}


void Triangle::Draw_Midpoint(Point Angle, SDL_Renderer *ren, Triangle ABC) {
	Point MidPoint;
	if (Angle.Name == "A") {
		MidPoint = ABC.Get_Midpoint(ABC.B, ABC.C);
	}
	else if (Angle.Name == "B") {
		MidPoint = ABC.Get_Midpoint(ABC.A, ABC.C);
	}
	else {
		MidPoint = ABC.Get_Midpoint(ABC.B, ABC.A);
	}
	Bresenham_Line(Angle.x, Angle.y, MidPoint.x, MidPoint.y, ren);
}

void Triangle::Draw_Straightforward(Point Angle1, Point Angle2,Point Angle3, SDL_Renderer *ren, Triangle ABC) {
	Point MidPoint;
	MidPoint = ABC.Get_Midpoint(Angle1, Angle2);
	float a_str, b_str, c_str, a, b;

	a = (float)Angle2.y - Angle1.y;
	b = (float)Angle1.x - Angle2.x;

	a_str = -b;
	b_str = a;
	c_str = (b * MidPoint.x) - (a * MidPoint.y);
	Bresenham_Line(MidPoint.x, MidPoint.y, 50, -(a_str * 50 + c_str) / b_str, ren);
}

void Triangle::Draw_Height(Point Angle, SDL_Renderer *ren,Point &gd) {
	float a_hei, b_hei, c_hei;
	Point temp;

	if (Angle.Name == "A") {
		a_hei = -b_BC;
		b_hei = a_BC;
		c_hei = (b_BC * A.x) - (a_BC * A.y);

		temp.x = ((b_hei*c_BC) - (b_BC*c_hei)) / ((a_hei*b_BC) - (a_BC*b_hei));
		temp.y = ((a_BC*c_hei) - (a_hei*c_BC)) / ((a_hei*b_BC) - (a_BC*b_hei));
		Bresenham_Line(A.x, A.y, temp.x, temp.y, ren);
	}
	else if (Angle.Name == "B") {
		a_hei = -b_AC;
		b_hei = a_AC;
		c_hei = (b_AC * B.x) - (a_AC * B.y);

		temp.x = ((b_hei*c_AC) - (b_AC*c_hei)) / ((a_hei*b_AC) - (a_AC*b_hei));
		temp.y = ((a_AC*c_hei) - (a_hei*c_AC)) / ((a_hei*b_AC) - (a_AC*b_hei));
		Bresenham_Line(B.x, B.y, temp.x, temp.y, ren);
	}
	else {
		a_hei = -b_AB;
		b_hei = a_AB;
		c_hei = (b_AB * C.x) - (a_AB * C.y);

		temp.x = ((b_hei*c_AB) - (b_AB*c_hei)) / ((a_hei*b_AB) - (a_AB*b_hei));
		temp.y = ((a_AB*c_hei) - (a_hei*c_AB)) / ((a_hei*b_AB) - (a_AB*b_hei));
		Bresenham_Line(C.x, C.y, temp.x, temp.y, ren);
	}
}

void Triangle::Draw_InscribedCircle(Point A, Point B, Point C, SDL_Renderer *ren,Point &incenter) {
	float a_bis1, b_bis1, c_bis1;
	float a_bis2, b_bis2, c_bis2;
	float r;
	Point center, temp;

	float d = sqrt((a_AB*a_AB) + (b_AB * b_AB)) / sqrt((a_AC*a_AC) + (b_AC * b_AC));
	a_bis1 = ((a_AC * d) - a_AB);
	b_bis1 = ((b_AC * d) - b_AB);
	c_bis1 = ((c_AC * d) - c_AB);
	if (((a_bis1 * B.x) + (b_bis1 * B.y) + c_bis1) * ((a_bis1 * C.x) + (b_bis1 * C.y) + c_bis1) > 0) {
		a_bis1 = ((a_AC * d) + a_AB);
		b_bis1 = ((b_AC * d) + b_AB);
		c_bis1 = ((c_AC * d) + c_AB);
	}

	d = sqrt((a_AB * a_AB) + (b_AB * b_AB)) / sqrt((a_BC * a_BC) + (b_BC * b_BC));
	a_bis2 = ((a_BC * d) - a_AB);
	b_bis2 = ((b_BC * d) - b_AB);
	c_bis2 = ((c_BC * d) - c_AB);
	if (((a_bis2 * A.x) + (b_bis2 * A.y) + c_bis2) * ((a_bis2 * C.x) + (b_bis2 * C.y) + c_bis2) > 0) {
		a_bis2 = ((a_BC * d) + a_AB);
		b_bis2 = ((b_BC * d) + b_AB);
		c_bis2 = ((c_BC * d) + c_AB);
	}

	center.x = ((b_bis1*c_bis2) - (b_bis2*c_bis1)) / ((a_bis1*b_bis2) - (a_bis2*b_bis1));
	center.y = ((a_bis2*c_bis1) - (a_bis1*c_bis2)) / ((a_bis1*b_bis2) - (a_bis2*b_bis1));

	float a_hei, b_hei, c_hei;
	a_hei = -b_BC;
	b_hei = a_BC;
	c_hei = (b_BC * center.x) - (a_BC * center.y);

	temp.x = ((b_hei*c_BC) - (b_BC*c_hei)) / ((a_hei*b_BC) - (a_BC*b_hei));
	temp.y = ((a_BC*c_hei) - (a_hei*c_BC)) / ((a_hei*b_BC) - (a_BC*b_hei));

	r = sqrt((temp.x - center.x)*(temp.x - center.x) + (temp.y - center.y)*(temp.y - center.y));

	BresenhamDrawCircle(center.x, center.y, r, ren);
	incenter.x = center.x;
	incenter.y = center.y;
}

void Triangle::Draw_Circumscribed(Point A, Point B, Point C, SDL_Renderer *ren, Triangle ABC,Point &ocenter) {
	Point MidPoint, center;

	float a_str1, b_str1, c_str1;
	float a_str2, b_str2, c_str2;
	float a1, b1, a2, b2;

	a1 = (float)A.y - B.y;
	b1 = (float)B.x - A.x;

	a2 = (float)B.y - C.y;
	b2 = (float)C.x - B.x;

	MidPoint = ABC.Get_Midpoint(A, B);

	//Cai khuc n�y l� viet phuong tirnh duong thang di qua Midpoint v� vuong goc voi duong AB
	a_str1 = -b1;
	b_str1 = a1;
	c_str1 = (b1 * MidPoint.x) - (a1 * MidPoint.y);

	MidPoint = ABC.Get_Midpoint(B, C);

	a_str2 = -b2;
	b_str2 = a2;
	c_str2 = (b2 * MidPoint.x) - (a2 * MidPoint.y);

	//Cai khuc nay la lay giao diem giua hai duong trung truc
	center.x = ((b_str1*c_str2) - (b_str2*c_str1)) / ((a_str1*b_str2) - (a_str2*b_str1));
	center.y = ((a_str2*c_str1) - (a_str1*c_str2)) / ((a_str1*b_str2) - (a_str2*b_str1));

	float R;
	R = sqrt((A.x - center.x)*(A.x - center.x) + (A.y - center.y)*(A.y - center.y));
	BresenhamDrawCircle(center.x, center.y, R, ren);
	ocenter.x = center.x;
	ocenter.y = center.y;
}