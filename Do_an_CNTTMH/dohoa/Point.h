#pragma once
#include<iostream>
#include<string>
#include<vector>
#include <SDL.h>
#include "Line.h"
#include "Circle.h"
using namespace std;

class Point
{
public:
	string Name;
	int x;
	int y;
	void Process_Triangle(Point A,Point B,Point C); //Tinh lai cac thanh phan
	void Draw_Bisectrix(Point A,Point B,Point C, SDL_Renderer *ren, Point &gd); //Ve duong phan giac

	Point Get_Midpoint(Point A, Point B);
	void Draw_Midpoint(Point A,Point B,Point C, SDL_Renderer *ren,Point &td); //Ve duong trung diem

	void Draw_Straightforward(Point A, Point B, Point &td, SDL_Renderer *ren); //Ve duong trung truc cua doan AB
	void Draw_Height(Point A, Point B, Point C, SDL_Renderer *ren, Point &gd); //Ve hinh chieu 
};