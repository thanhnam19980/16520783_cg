#include <iostream>
#include<string>
#include <SDL.h>
#include<map>
#include<vector>
#include<SDL_ttf.h>
#include"Rectangle.h"
#include"Tron.h"
#include "Line.h"
#include "Circle.h"
#include"Triangle.h"
#include"Square.h"
#include "Vector2D.h"
#include"Point.h"
#include "Matrix2D.h"
using namespace std;
int buoc1(vector<string> &a)
{
	if (a[0] == "goi")
	{
		a.erase(a.begin());
		if (a[2] == "trung")
		{
			a.erase(a.begin() + 1, a.begin() + 4);
			return 1;
		}
		if (a[2] == "hinh")
		{
			a.erase(a.begin() + 1, a.begin() + 5);
			a.erase(a.begin() + 2);
			return 36;
		}
		if (a[2] == "diem")
		{
			if (a[3] == "thuoc")
			{
				a.erase(a.begin() + 1, a.end());
				return 2;
			}
			a.erase(a.begin() + 1, a.end());
			return 3;
		}
		if (a[4] == "tiep")
		{
			a.erase(a.begin() + 1);
			a.erase(a.begin() + 2, a.end());
			return 4;
		}
	}
	if (a[0] == "tam")
	{
		a.erase(a.begin(), a.begin() + 2);
		if (a.size() == 1)
			return 15;
		if (a[1] == "can")
		{
			a.erase(a.begin() + 1, a.begin() + 3);
			return 11;
		}
		if (a[1] == "vuong")
		{
			if (a[2] == "can")
			{
				a.erase(a.begin() + 1, a.begin() + 4);
				return 12;
			}
			a.erase(a.begin() + 1, a.begin() + 3);
			return 13;
		}
		if (a[0] == "deu" || a[1] == "deu")
		{
			if (a[0] == "deu")
				a.erase(a.begin());
			else
				a.erase(a.begin() + 1);
			return 14;
		}
	}
	if (a[0] == "hinh")
	{
		a.erase(a.begin());
		if (a[0] == "chu")
		{
			a.erase(a.begin(), a.begin() + 2);
			return 21;
		}
		if (a[0] == "vuong")
		{
			a.erase(a.begin());
			return 22;
		}
	}
	if (a[0] == "duong")
	{
		a.erase(a.begin());
		if (a[0] == "kinh")
		{
			a.erase(a.begin());
			return 37;
		}
		if (a[0] == "tron")
		{
			a.erase(a.begin(), a.begin() + 2);
			return 31;
		}
		if (a[0] == "cao")
		{
			a.erase(a.begin());
			a.erase(a.begin() + 1, a.begin() + 4);
			return 32;
		}
		if (a[0] == "phan")
		{
			a.erase(a.begin(), a.begin() + 2);
			a.erase(a.begin() + 1, a.begin() + 3);
			return 33;
		}
		if (a[0] == "trung")
		{
			a.erase(a.begin());
			if (a[0] == "tuyen")
			{
				a.erase(a.begin());
				a.erase(a.begin() + 1, a.begin() + 4);
				return 34;
			}
			a.erase(a.begin());
			a.erase(a.begin() + 1);
			return 35;
		}
	}
	return 0;
}
void vetendinh(Point a, TTF_Font *font, SDL_Texture* texture, SDL_Color mau, SDL_Renderer* ren, SDL_Rect &srcRest, SDL_Rect &desRect)
{
	SDL_Surface* surface = TTF_RenderText_Solid(font, a.Name.c_str(), mau);
	texture = SDL_CreateTextureFromSurface(ren, surface);
	SDL_FreeSurface(surface);
	TTF_SizeText(font, a.Name.c_str(), &srcRest.w, &srcRest.h);
	srcRest.x = 0;
	srcRest.y = 0;
	if (a.x > 550)
		desRect.x = a.x + 10;
	else
		desRect.x = a.x - 10;
	if (a.y > 300)
		desRect.y = a.y + 10;
	else
		desRect.y = a.y - 10;
	desRect.w = srcRest.w;
	desRect.h = srcRest.h;
	SDL_RenderCopy(ren, texture, &srcRest, &desRect);
}
int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}
	//Initialize the truetype font API.
	if (TTF_Init() < 0)
	{
		SDL_Log("%s", TTF_GetError());
		return -1;
	}


	//Now create a window with title "Hello World" at 10, 10 on the screen with w:800 h:600 and show it
	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 30, 1365, 680, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == nullptr) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//video driver supports the flags we're passing
	//Flags: SDL_RENDERER_ACCELERATED: We want to use hardware accelerated rendering
	//SDL_RENDERER_PRESENTVSYNC: We want the renderer's present function (update screen) to be
	//synchronized with the monitor's refresh rate
	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == nullptr) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}
	TTF_Font *font = TTF_OpenFont("VeraMoBd.ttf", 15);
	SDL_Color maudinh = { 243, 156, 18 };
	SDL_Texture* texture = NULL;
	SDL_Rect srcRest;
	SDL_Rect desRect;

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	string s;
	vector<vector<string>> de;
	vector<string> a;
	string t;
	cin >> s;
	while (s != ".")
	{
		if (s == "tam" || s == "hinh" || s == "duong" || s == "goi")
		{
			a.resize(0);
			a.push_back(s);
			cin >> s;
			while (s != "." && s != ",")
			{
				a.push_back(s);
				cin >> s;
			}
			de.push_back(a);
		}
		else
			cin >> s;
	}
	Point tam;
	char tt;
	Point t1, t2, t3, t4;
	t1.x = 700;
	t1.y = 100;
	t2.y = t3.y = 420;
	Triangle ABC;
	Square sABCD;
	rectangle rABCD;
	Circle O;
	int loai;
	map<string, Point> list;
	for (int i = 0; i < de.size(); i++)
	{
		loai = buoc1(de[i]);
		switch (loai)
		{
		case 1:
		{
			//lay td
			t2.Name = de[i][1][0];
			t3.Name = de[i][1][1];
			t1 = t2.Get_Midpoint(t2, t3);
			t1.Name = de[i][0];
			list[tam.Name] = tam;
			SDL_RenderDrawPoint(ren, tam.x, tam.y);
			vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 2:
		{
			//lay A thuoc O
			tam = O.Get_PointInCir(de[i][0]);
			list[tam.Name] = tam;
			SDL_RenderDrawPoint(ren, tam.x, tam.y);
			vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 3:
		{
			//lay A ngoai O
			tam = O.Get_PointOutCir(de[i][0]);
			list[tam.Name] = tam;
			SDL_RenderDrawPoint(ren, tam.x, tam.y);
			vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 4:
		{
			//ve tiep tuyen
			t = de[i][0][0];
			tam = list[t];
			O.Draw_Tangent(tam, t1, t2, ren);
			t1.Name = de[i][0][1];
			t2.Name = de[i][1][1];
			list[t1.Name] = t1;
			list[t2.Name] = t2;
			vetendinh(t1, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(t2, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 11:
		{
			//tamgiaccan
			t1.Name = de[i][1];
			tt = t1.Name[0];
			for (int j = 0; j < 3; j++)
			{
				if (de[i][0][j] == tt)
				{
					de[i][0].erase(de[i][0].begin() + j);
					break;
				}
			}
			t2.Name = de[i][0][0];
			t3.Name = de[i][0][1];
			t2.x = 460;
			t3.x = 940;
			ABC.B = t2;
			ABC.C = t3;
			ABC.A = t1;
			list[t1.Name] = t1;
			list[t2.Name] = t2;
			list[t3.Name] = t3;
			ABC.Process_Triangle();
			ABC.Draw_Triangle(ABC.A, ABC.B, ABC.C, ren);
			vetendinh(ABC.A, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(ABC.B, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(ABC.C, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 12:
		{
			//tamgiacvuongcan
			t2.Name = de[i][1];
			tt = t1.Name[0];
			for (int j = 0; j < 3; j++)
			{
				if (de[i][0][j] == tt)
				{
					de[i][0].erase(de[i][0].begin() + j);
					break;
				}
			}
			t1.Name = de[i][0][0];
			t2.x = 700;
			t3.x = 1020;
			ABC.B = t2;
			ABC.C = t3;
			ABC.A = t1;
			list[t1.Name] = t1;
			list[t2.Name] = t2;
			list[t3.Name] = t3;
			ABC.Process_Triangle();
			ABC.Draw_Triangle(ABC.A, ABC.B, ABC.C, ren);
			vetendinh(ABC.A, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(ABC.B, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(ABC.C, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 13:
		{
			//tgvuong
			t2.Name = de[i][1];
			tt = t2.Name[0];
			for (int j = 0; j < 3; j++)
			{
				if (de[i][0][j] == tt)
				{
					de[i][0].erase(de[i][0].begin() + j);
					break;
				}
			}
			t1.Name = de[i][0][0];
			t3.Name = de[i][0][1];
			t2.x = 700;
			t3.x = 1150;
			ABC.B = t2;
			ABC.C = t3;
			ABC.A = t1;
			list[t1.Name] = t1;
			list[t2.Name] = t2;
			list[t3.Name] = t3;
			ABC.Process_Triangle();
			ABC.Draw_Triangle(ABC.A, ABC.B, ABC.C, ren);
			vetendinh(ABC.A, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(ABC.B, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(ABC.C, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 14:
		{
			//tgdeu
			t1.Name = de[i][0][0];
			t2.Name = de[i][0][1];
			t3.Name = de[i][0][2];
			t2.x = 515;
			t3.x = 885;
			ABC.B = t2;
			ABC.C = t3;
			ABC.A = t1;
			list[t1.Name] = t1;
			list[t2.Name] = t2;
			list[t3.Name] = t3;
			ABC.Process_Triangle();
			ABC.Draw_Triangle(ABC.A, ABC.B, ABC.C, ren);
			vetendinh(ABC.A, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(ABC.B, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(ABC.C, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 15:
		{
			t1.Name = de[i][0][0];
			t2.Name = de[i][0][1];
			t3.Name = de[i][0][2];
			t2.x = 400;
			t2.y = 500;
			t3.x = 850;
			ABC.B = t2;
			ABC.C = t3;
			ABC.A = t1;
			list[t1.Name] = t1;
			list[t2.Name] = t2;
			list[t3.Name] = t3;
			ABC.Process_Triangle();
			ABC.Draw_Triangle(ABC.A, ABC.B, ABC.C, ren);
			vetendinh(ABC.A, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(ABC.B, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(ABC.C, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 21:
		{
			//chunhat
			t1.Name = de[i][0][0];
			t2.Name = de[i][0][1];
			t3.Name = de[i][0][2];
			t4.Name = de[i][0][3];
			rABCD.A.Name = t1.Name;
			rABCD.B.Name = t2.Name;
			rABCD.C.Name = t3.Name;
			rABCD.D.Name = t4.Name;
			list[rABCD.A.Name] = rABCD.A;
			list[rABCD.B.Name] = rABCD.B;
			list[rABCD.C.Name] = rABCD.C;
			list[rABCD.D.Name] = rABCD.D;
			rABCD.Draw_Rectangle(rABCD.A, rABCD.B, rABCD.C, rABCD.D, ren);
			vetendinh(rABCD.A, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(rABCD.B, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(rABCD.C, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(rABCD.D, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 22:
		{
			//vuong
			t1.Name = de[i][0][0];
			t2.Name = de[i][0][1];
			if (list.find(t1.Name) != list.end() && list.find(t2.Name) != list.end()) //2 diem co r
			{
				t3 = list[t1.Name];
				t4 = list[t2.Name];
				sABCD.Draw_Square(t3, t4, de[i][0], ren);
				list[sABCD.C.Name] = sABCD.C;
				list[sABCD.D.Name] = sABCD.D;
			}
			else
			{
				t3.Name = de[i][0][2];
				t4.Name = de[i][0][3];
				sABCD.A.Name = t1.Name;
				sABCD.B.Name = t2.Name;
				sABCD.C.Name = t3.Name;
				sABCD.D.Name = t4.Name;
				list[sABCD.A.Name] = sABCD.A;
				list[sABCD.B.Name] = sABCD.B;
				list[sABCD.C.Name] = sABCD.C;
				list[sABCD.D.Name] = sABCD.D;
				sABCD.Draw_Square(sABCD.A, sABCD.B, sABCD.C, sABCD.D, ren);
			}
			vetendinh(sABCD.A, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(sABCD.B, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(sABCD.C, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(sABCD.D, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 31:
		{
			//dtron
			tam.Name = t1.Name = de[i][0];
			if (de[i].size() > 1)
			{
				if (de[i][1] == "ngoai")
				{
					de[i].erase(de[i].begin(), de[i].begin() + 3);
					if (de[i][0] == "tam") //ngoaitieptamgiac
					{
						de[i].erase(de[i].begin(), de[i].begin() + 2);
						ABC.Draw_Circumscribed(ABC.A, ABC.B, ABC.C, ren, ABC, tam);
						vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
						SDL_RenderPresent(ren);
					}
					else
					{
						if (de[i][1] == "vuong")
						{
							de[i].erase(de[i].begin(), de[i].begin() + 2);
							sABCD.Draw_Circumscribed(ren);
							tam = sABCD.Get_Center();
							tam.Name = t1.Name;
							list[tam.Name] = tam;
							vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
							SDL_RenderPresent(ren);
						}
						else //hcn
						{
							de[i].erase(de[i].begin(), de[i].begin() + 3);
							rABCD.Draw_Circumscribed(ren);
							tam = rABCD.Get_Center();
							tam.Name = t1.Name;
							list[tam.Name] = tam;
							vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
							SDL_RenderPresent(ren);
						}
					}
				}
				else // noi tiep
				{
					de[i].erase(de[i].begin(), de[i].begin() + 5);
					ABC.Draw_InscribedCircle(ABC.A, ABC.B, ABC.C, ren, tam);
					list[tam.Name] = tam;
					vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
					SDL_RenderPresent(ren);
				}
			}
			else
			{
				O.center.Name = de[i][0];
				O.Draw_Circle(ren);
				tam = O.center;
				list[tam.Name] = tam;
				vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
				SDL_RenderPresent(ren);
			}
			break;
		}
		case 32:
		{
			//cao
			t = de[i][0][0];
			tt = de[i][0][0];
			t1 = list[t];
			for (int j = 0; j < 3; j++)
			{
				if (de[i][1][j] == tt)
				{
					de[i][1].erase(de[i][1].begin() + j);
					break;
				}
			}
			t = de[i][1][0];
			t2 = list[t];
			t = de[i][1][1];
			t3 = list[t];
			t1.Draw_Height(t1, t2, t3, ren, tam);
			tam.Name = de[i][0][1];
			list[tam.Name] = tam;
			vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 33:
		{
			//pg
			t = de[i][0][0];
			t1 = list[t];
			t = de[i][1][0];
			t2 = list[t];
			t = de[i][1][2];
			t3 = list[t];
			t1.Draw_Bisectrix(t1, t2, t3, ren, tam);
			tam.Name = de[i][0][1];
			list[tam.Name] = tam;
			vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 34:
		{
			//tuyen
			t = de[i][0][0];
			tt = de[i][0][0];
			t1 = list[t];
			for (int j = 0; j < 3; j++)
			{
				if (de[i][1][j] == tt)
				{
					de[i][1].erase(de[i][1].begin() + j);
					break;
				}
			}
			t = de[i][1][0];
			t2 = list[t];
			t = de[i][1][1];
			t3 = list[t];
			t1.Draw_Midpoint(t1, t2, t3, ren, tam);
			tam.Name = de[i][0][1];
			list[tam.Name] = tam;
			vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 35:
		{
			//truc
			t = de[i][1][0];
			t2 = list[t];
			t = de[i][1][1];
			t3 = list[t];
			t2.Draw_Straightforward(t2, t3, tam, ren);
			tam.Name = de[i][0][0];
			list[tam.Name] = tam;
			vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 36:
		{
			//hinh chieu H A BD
			t = de[i][1];
			t1 = list[t];
			t = de[i][2][0];
			t2 = list[t];
			t = de[i][2][1];
			t3 = list[t];
			t1.Draw_Height(t1, t2, t3, ren, tam);
			DDA_Line(t2.x, t2.y, t3.x, t3.y, ren);
			tam.Name = de[i][0];
			list[tam.Name] = tam;
			vetendinh(tam, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		case 37:
		{
			//duongkinh
			t1.Name = de[i][0][0];
			t2.Name = de[i][0][1];
			O.Draw_Diameter(ren, t1, t2);
			list[t1.Name] = t1;
			list[t2.Name] = t2;
			vetendinh(t1, font, texture, maudinh, ren, srcRest, desRect);
			vetendinh(t2, font, texture, maudinh, ren, srcRest, desRect);
			SDL_RenderPresent(ren);
			break;
		}
		default:
			break;
		}
		system("pause");
	}

	//draw to screen
	SDL_RenderPresent(ren);
	system("pause");

	//Take a quick break after all that hard work
	//Quit if happen QUIT event

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}