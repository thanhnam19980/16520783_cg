#include"Tron.h"

Circle::Circle() {
	center.Name = 'O';
	center.x = 500;
	center.y = 300;
	r = 200;
}

Point Circle::Get_PointInCir(string name) {
	Point temp;
	temp.Name = name;
	temp.x = 300;
	temp.y = sqrt((this->r*this->r) - (temp.x - this->center.x)*(temp.x - this->center.x)) + this->center.y;
	return temp;
}

Point Circle::Get_PointOutCir(string name)
{
	Point temp;
	temp.Name = name;
	temp.x = 100;
	temp.y = 100;
	return temp;
}

void Circle::Draw_Diameter(SDL_Renderer *ren, Point &a, Point &b) {
	a = this->Get_PointInCir(a.Name);
	b.x = 2 * this->center.x - a.x;
	b.y = 2 * this->center.y - a.y;
	Bresenham_Line(a.x, a.y, b.x, b.y, ren);
}

void Circle::Draw_Circle(SDL_Renderer *ren) {
	SDL_RenderDrawPoint(ren, this->center.x, this->center.y);
	BresenhamDrawCircle(this->center.x, this->center.y, this->r, ren);
}

void Circle::Draw_Tangent(Point A,Point &gd1,Point &gd2, SDL_Renderer *ren) {
	float* m = Solve(((this->center.x - A.x)*(this->center.x - A.x) - this->r*this->r), 2 * (this->center.x - A.x)*(A.y - this->center.y), (A.y - this->center.y)*(A.y - this->center.y) - this->r*this->r);
	if (m[0] == -1 || m[1] == -1)
		return;

	float a_tan, b_tan, c_tan;
	float a_hei, b_hei, c_hei;

	a_tan = m[0];
	b_tan = -1;
	c_tan = -m[0] * A.x + A.y;

	a_hei = -b_tan;
	b_hei = a_tan;
	c_hei = (b_tan * this->center.x) - (a_tan * this->center.y);

	Point temp;
	temp.x = ((b_hei*c_tan) - (b_tan*c_hei)) / ((a_hei*b_tan) - (a_tan*b_hei));
	temp.y = ((a_tan*c_hei) - (a_hei*c_tan)) / ((a_hei*b_tan) - (a_tan*b_hei));
	gd1 = temp;
	Bresenham_Line(A.x, A.y, temp.x, temp.y, ren);
	Bresenham_Line(center.x, center.y, temp.x, temp.y, ren);

	a_tan = m[1];
	b_tan = -1;
	c_tan = -m[1] * A.x + A.y;

	a_hei = -b_tan;
	b_hei = a_tan;
	c_hei = (b_tan * this->center.x) - (a_tan * this->center.y);

	temp.x = ((b_hei*c_tan) - (b_tan*c_hei)) / ((a_hei*b_tan) - (a_tan*b_hei));
	temp.y = ((a_tan*c_hei) - (a_hei*c_tan)) / ((a_hei*b_tan) - (a_tan*b_hei));
	gd2 = temp;
	Bresenham_Line(A.x, A.y, temp.x, temp.y, ren);
	Bresenham_Line(center.x, center.y, temp.x, temp.y, ren);

	Bresenham_Line(A.x, A.y, center.x, center.y, ren);
}