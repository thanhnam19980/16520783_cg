#pragma once
#include "Point.h"
#include "Line.h"
#include "Circle.h"

class rectangle {
public:
	Point A, B, C, D;
	float AB, BC, CD, DA;

	float a_AB, b_AB, c_AB;
	float a_BC, b_BC, c_BC;
	float a_CD, b_CD, c_CD;
	float a_DA, b_DA, c_DA;
public:
	rectangle();
	Point Get_MidPoint(Point A, Point B); //Lay trung diem
	Point Get_MidPoint(Point A, Point B, string name); //Lay trung diem co ten
	Point Get_Center(); //Lay giao diem hai duong cheo cua hinh vuong

	void Draw_Rectangle(Point A, Point B, Point C, Point D, SDL_Renderer *ren); //Ve hinh chu nhat
	void Draw_Projection(Point A, Point B, Point C, SDL_Renderer *ren); //Ve hinh chieu cua A len BC v� ve luon duong BC
	void Draw_Circumscribed(SDL_Renderer *ren); //ve duong tron ngoai tiep hinh chu nhat
};

