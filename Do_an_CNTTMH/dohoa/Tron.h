#pragma once
#include<SDL.h>
#include"Point.h"
#include"Line.h"
#include"Circle.h"


class Circle {
public:
	Point center;
	float r;
public:
	Circle();

	Point Get_PointInCir(string name);  //Lay mot diem thuoc duong tron, truyen vao ten cua diem do
	Point Get_PointOutCir(string name);  //Lay mot diem thuoc duong tron, truyen vao ten cua diem do

	void Draw_Diameter(SDL_Renderer *ren, Point &a, Point &b); //Ve duong kinh cua duong tron
	void Draw_Circle(SDL_Renderer *ren);
	void Draw_Tangent(Point A,Point &gd1,Point &gd2, SDL_Renderer *ren); //Ve tiep tuyen cua duong tron
};