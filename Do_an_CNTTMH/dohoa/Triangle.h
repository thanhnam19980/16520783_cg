#pragma once
#include<iostream>
#include<string>
#include<vector>
#include <SDL.h>
#include "Line.h"
#include "Circle.h"
#include"Point.h"
using namespace std;


class Triangle {
public:
	//3 dinh
	Point A, B, C;
	float a, b, c;
	float Ang_A, Ang_B, Ang_C;

	float a_AC, b_AC, c_AC;
	float a_AB, b_AB, c_AB;
	float a_BC, b_BC, c_BC;
public:
	Triangle();
	~Triangle() {}

	void Get_Input(vector<string> input, int n);
	void Process_Triangle(); //Tinh lai cac thanh phan cua tam giac khi biet diem

	void Draw_Triangle(Point A, Point B, Point C, SDL_Renderer *ren);
	void Draw_Bisectrix(Point Angle, SDL_Renderer *ren, Point &gd); //Ve duong phan giac

	Point Get_Midpoint(Point A, Point B); //Lay trung diem cua doan AB

	void Draw_Midpoint(Point Angle, SDL_Renderer *ren, Triangle ABC); //Ve duong trung diem
	void Draw_Straightforward(Point Angle1, Point Angle2, Point Angle3, SDL_Renderer *ren, Triangle ABC); //Ve duong trung truc cua doan AB
	void Draw_Height(Point Angle, SDL_Renderer *ren, Point &gd); //Ve duong cao qua dinh A

	void Draw_InscribedCircle(Point A, Point B, Point C, SDL_Renderer *ren, Point &incenter); //Ve duong trong noi tiep tam giac
	void Draw_Circumscribed(Point A, Point B, Point C, SDL_Renderer *ren, Triangle ABC, Point &ocenter); //Ve duong trong ngoai tiep tam giac
};
