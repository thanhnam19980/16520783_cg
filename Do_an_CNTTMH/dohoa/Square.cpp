#include "Square.h"

Square::Square() {
	A.x = 300, A.y = 200;
	B.x = 600, B.y = 200;
	C.x = 600, C.y = 500;
	D.x = 300, D.y = 500;

	AB = sqrt((A.x - B.x)*(A.x - B.x) + (A.y - B.y)*(A.y - B.y));
	BC = sqrt((B.x - C.x)*(B.x - C.x) + (B.y - C.y)*(B.y - C.y));
	CD = sqrt((C.x - D.x)*(C.x - D.x) + (C.y - D.y)*(C.y - D.y));
	DA = sqrt((D.x - A.x)*(D.x - A.x) + (D.y - A.y)*(D.y - A.y));

	a_AB = B.y - A.y;
	b_AB = A.x - B.x;
	c_AB = -A.x*B.y + B.x*A.y;

	a_BC = C.y - B.y;
	b_BC = B.x - C.x;
	c_BC = -B.x*C.y + B.x*C.y;

	a_CD = D.y - C.y;
	b_CD = D.x - C.x;
	c_CD = -C.x*D.y + C.x*D.y;

	a_DA = A.y - D.y;
	b_DA = D.x - A.x;
	c_DA = -D.x*A.y + D.x*A.y;
}

Point Square::Get_MidPoint(Point A, Point B) {
	Point temp;
	temp.x = (A.x + B.x) / 2;
	temp.y = (A.y + B.y) / 2;
	return temp;
}

Point Square::Get_MidPoint(Point A, Point B, string name) {
	Point temp;
	temp.Name = name;
	temp.x = (A.x + B.x) / 2;
	temp.y = (A.y + B.y) / 2;
	return temp;
}

Point Square::Get_Center() {
	Point temp;
	float a_AC, b_AC, c_AC;
	float a_BD, b_BD, c_BD;

	a_AC = C.y - A.y;
	b_AC = A.x - C.x;
	c_AC = -A.x*C.y + C.x*A.y;

	a_BD = D.y - B.y;
	b_BD = B.x - D.x;
	c_BD = -B.x*D.y + D.x*B.y;


	temp.x = ((b_AC*c_BD) - (b_BD*c_AC)) / ((a_AC*b_BD) - (a_BD*b_AC));
	temp.y = ((a_BD*c_AC) - (a_AC*c_BD)) / ((a_AC*b_BD) - (a_BD*b_AC));

	return temp;
}

void Square::Draw_Square(Point Angle1, Point Angle2, string name, SDL_Renderer *ren) {
	//Cap nhat lai cac toa do dinh cua hinh vuong
	this->A = Angle1;
	this->B = Angle2;

	float a_Edge = Angle2.y - Angle1.y;
	float b_Edge = Angle1.x - Angle2.x;
	float c_Edge = -Angle1.x*Angle2.y + Angle2.x*Angle1.y;

	float a_hei1, b_hei1, c_hei1;
	float a_hei2, b_hei2, c_hei2;

	a_hei1 = -b_Edge;
	b_hei1 = a_Edge;
	c_hei1 = (b_Edge * Angle1.x) - (a_Edge * Angle1.y);

	a_hei2 = -b_Edge;
	b_hei2 = a_Edge;
	c_hei2 = (b_Edge * Angle2.x) - (a_Edge * Angle2.y);

	int d = sqrt((Angle1.x - Angle2.x)*(Angle1.x - Angle2.x) + (Angle1.y - Angle2.y)*(Angle1.y - Angle2.y));

	Point temp1, temp2;
	
	if (b_hei1 != 0) {
		temp1.x = ((b_Edge*c_hei1) - (b_hei1*c_Edge) + (d*b_hei1)*sqrt((a_Edge*a_Edge) + (b_Edge*b_Edge))) / ((a_Edge*b_hei1) - (a_hei1*b_Edge));
		if (temp1.x < 0)
			temp1.x = ((b_Edge*c_hei1) - (b_hei1*c_Edge) - (d*b_hei1)*sqrt((a_Edge*a_Edge) + (b_Edge*b_Edge))) / ((a_Edge*b_hei1) - (a_hei1*b_Edge));
		temp1.y = ((-a_hei1 * temp1.x) - c_hei1) / b_hei1;
	}
	else {
		temp1.x = -c_hei1 / a_hei1;
		temp1.y = ((a_hei1*c_hei1) - (a_Edge*c_Edge) + d * sqrt((a_Edge*a_Edge) + (b_Edge*b_Edge))) / (a_Edge*b_Edge);
		if (temp1.y < 0)
			temp1.y = ((a_hei1*c_hei1) - (a_Edge*c_Edge) - d * sqrt((a_Edge*a_Edge) + (b_Edge*b_Edge))) / (a_Edge*b_Edge);
	}

	if (b_hei2 != 0) {
		temp2.x = ((b_Edge*c_hei2) - (b_hei2*c_Edge) + (d*b_hei2)*sqrt((a_Edge*a_Edge) + (b_Edge*b_Edge))) / ((a_Edge*b_hei2) - (a_hei2*b_Edge));
		if (temp2.x < 0)
			temp2.x = ((b_Edge*c_hei2) - (b_hei2*c_Edge) - (d*b_hei2)*sqrt((a_Edge*a_Edge) + (b_Edge*b_Edge))) / ((a_Edge*b_hei2) - (a_hei2*b_Edge));
		temp2.y = ((-a_hei2 * temp2.x) - c_hei2) / b_hei2;
	}
	else {
		temp2.x = -c_hei2 / a_hei2;
		temp2.y = ((a_hei2*c_hei2) - (a_Edge*c_Edge) + d * sqrt((a_Edge*a_Edge) + (b_Edge*b_Edge))) / (a_Edge*b_Edge);
		if (temp2.y < 0)
			temp2.y = ((a_hei2*c_hei2) - (a_Edge*c_Edge) - d * sqrt((a_Edge*a_Edge) + (b_Edge*b_Edge))) / (a_Edge*b_Edge);
	}

	temp1.Name = name[3];
	temp2.Name = name[2];

	this->C = temp2;
	this->D = temp1;

	this->Draw_Square(this->A, this->B, this->C, this->D, ren);
}

void Square::Draw_Square(Point A, Point B, Point C, Point D, SDL_Renderer *ren) {
	Bresenham_Line(A.x, A.y, B.x, B.y, ren);
	Bresenham_Line(B.x, B.y, C.x, C.y, ren);
	Bresenham_Line(C.x, C.y, D.x, D.y, ren);
	Bresenham_Line(D.x, D.y, A.x, A.y, ren);
}

void Square::Draw_InscribedCircle(SDL_Renderer *ren) {
	Point temp, center;
	temp = this->Get_MidPoint(this->A, this->B);
	center = this->Get_Center();

	float r = sqrt((center.x - temp.x)*(center.x - temp.x) + (center.y - temp.y)*(center.y - temp.y));
	BresenhamDrawCircle(center.x, center.y, r, ren);
}

void Square::Draw_Circumscribed(SDL_Renderer *ren) {
	Point center;
	center = this->Get_Center();
	float r = sqrt((center.x - A.x)*(center.x - A.x) + (center.y - A.y)*(center.y - A.y));
	BresenhamDrawCircle(center.x, center.y, r, ren);
}