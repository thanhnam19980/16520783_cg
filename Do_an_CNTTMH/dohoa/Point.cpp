#include "Point.h"


void Point::Draw_Bisectrix(Point A, Point B, Point C, SDL_Renderer * ren, Point &gd)
{
	float a_bis, b_bis,c_bis;

	float a, b, c;
	float Ang_A, Ang_B, Ang_C;
	float a_AC, b_AC, c_AC;
	float a_AB, b_AB, c_AB;
	float a_BC, b_BC, c_BC;
	a = sqrt((C.x - B.x)*(C.x - B.x) + (C.y - B.y)*(C.y - B.y));
	b = sqrt((A.x - C.x)*(A.x - C.x) + (A.y - C.y)*(A.y - C.y));
	c = sqrt((A.x - B.x)*(A.x - B.x) + (A.y - B.y)*(A.y - B.y));

	Ang_A = acos(float((b*b + c * c - a * a) / (2 * b*c)));
	Ang_B = acos(float((a*a + c * c - b * b) / (2 * a*c)));
	Ang_C = acos(float((a*a + b * b - c * c) / (2 * b*a)));

	a_AB = B.y - A.y;
	b_AB = A.x - B.x;
	c_AB = -A.x*B.y + B.x*A.y;

	a_BC = C.y - B.y;
	b_BC = B.x - C.x;
	c_BC = -B.x*C.y + C.x*B.y;

	a_AC = C.y - A.y;
	b_AC = A.x - C.x;
	c_AC = -A.x*C.y + C.x*A.y;

	Point temp;
		float d = sqrt((a_AB*a_AB) + (b_AB * b_AB)) / sqrt((a_AC*a_AC) + (b_AC * b_AC));
		a_bis = ((a_AC * d) - a_AB);
		b_bis = ((b_AC * d) - b_AB);
		c_bis = ((c_AC * d) - c_AB);
		if (((a_bis * B.x) + (b_bis * B.y) + c_bis) * ((a_bis * C.x) + (b_bis * C.y) + c_bis) > 0) {
			a_bis = ((a_AC * d) + a_AB);
			b_bis = ((b_AC * d) + b_AB);
			c_bis = ((c_AC * d) + c_AB);
		}

		temp.x = ((b_bis*c_BC) - (b_BC*c_bis)) / ((a_bis*b_BC) - (a_BC*b_bis));
		temp.y = ((a_BC*c_bis) - (a_bis*c_BC)) / ((a_bis*b_BC) - (a_BC*b_bis));

		Bresenham_Line(A.x, A.y, temp.x, temp.y, ren);
	gd = temp;
}

Point Point::Get_Midpoint(Point A, Point B)
{
	Point MidPoint;
	MidPoint.x = (A.x + B.x) / 2;
	MidPoint.y = (A.y + B.y) / 2;
	return MidPoint;
}

void Point::Draw_Midpoint(Point A,Point B,Point C, SDL_Renderer * ren,Point &td)
{
	Point MidPoint = A.Get_Midpoint(B, C);
	Bresenham_Line(A.x, A.y, MidPoint.x, MidPoint.y, ren);
	td = MidPoint;
}

void Point::Draw_Straightforward(Point A, Point B, Point &td, SDL_Renderer * ren)
{
	Point MidPoint;
	MidPoint = Get_Midpoint(A, B);
	float a_str, b_str, c_str, a, b;

	a = (float)B.y - A.y;
	b = (float)A.x - B.x;

	a_str = -b;
	b_str = a;
	c_str = (b * MidPoint.x) - (a * MidPoint.y);
	Bresenham_Line(MidPoint.x, MidPoint.y, 50, -(a_str * 50 + c_str) / b_str, ren);
	td = MidPoint;
}

void Point::Draw_Height(Point A, Point B, Point C, SDL_Renderer * ren, Point &gd)
{
	float a, b, c;
	float Ang_A, Ang_B, Ang_C;
	float a_AC, b_AC, c_AC;
	float a_AB, b_AB, c_AB;
	float a_BC, b_BC, c_BC;
	a = sqrt((C.x - B.x)*(C.x - B.x) + (C.y - B.y)*(C.y - B.y));
	b = sqrt((A.x - C.x)*(A.x - C.x) + (A.y - C.y)*(A.y - C.y));
	c = sqrt((A.x - B.x)*(A.x - B.x) + (A.y - B.y)*(A.y - B.y));

	Ang_A = acos(float((b*b + c * c - a * a) / (2 * b*c)));
	Ang_B = acos(float((a*a + c * c - b * b) / (2 * a*c)));
	Ang_C = acos(float((a*a + b * b - c * c) / (2 * b*a)));

	a_AB = B.y - A.y;
	b_AB = A.x - B.x;
	c_AB = -A.x*B.y + B.x*A.y;

	a_BC = C.y - B.y;
	b_BC = B.x - C.x;
	c_BC = -B.x*C.y + C.x*B.y;

	a_AC = C.y - A.y;
	b_AC = A.x - C.x;
	c_AC = -A.x*C.y + C.x*A.y;
	float a_hei, b_hei, c_hei;
	Point temp;
		a_hei = -b_BC;
		b_hei = a_BC;
		c_hei = (b_BC * A.x) - (a_BC * A.y);

		temp.x = ((b_hei*c_BC) - (b_BC*c_hei)) / ((a_hei*b_BC) - (a_BC*b_hei));
		temp.y = ((a_BC*c_hei) - (a_hei*c_BC)) / ((a_hei*b_BC) - (a_BC*b_hei));
		Bresenham_Line(A.x, A.y, temp.x, temp.y, ren);
	gd = temp;
}
