#pragma once
#include<SDL.h>
#include"Point.h"
#include"Line.h"
#ifndef CIRCLE_H
#define CIRCLE_H
void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren);
void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren);
float* Solve(float a, float b, float c);
#endif //GRAPHICS2D_CIRCLE_H