#include "rectangle.h"

rectangle::rectangle() {
	A.x = 200, A.y = 200;
	B.x = 700, B.y = 200;
	C.x = 700, C.y = 500;
	D.x = 200, D.y = 500;

	AB = sqrt((A.x - B.x)*(A.x - B.x) + (A.y - B.y)*(A.y - B.y));
	BC = sqrt((B.x - C.x)*(B.x - C.x) + (B.y - C.y)*(B.y - C.y));
	CD = sqrt((C.x - D.x)*(C.x - D.x) + (C.y - D.y)*(C.y - D.y));
	DA = sqrt((D.x - A.x)*(D.x - A.x) + (D.y - A.y)*(D.y - A.y));

	a_AB = B.y - A.y;
	b_AB = A.x - B.x;
	c_AB = -A.x*B.y + B.x*A.y;

	a_BC = C.y - B.y;
	b_BC = B.x - C.x;
	c_BC = -B.x*C.y + B.x*C.y;

	a_CD = D.y - C.y;
	b_CD = D.x - C.x;
	c_CD = -C.x*D.y + C.x*D.y;

	a_DA = A.y - D.y;
	b_DA = D.x - A.x;
	c_DA = -D.x*A.y + D.x*A.y;
}

Point rectangle::Get_MidPoint(Point A, Point B) {
	Point temp;
	temp.x = (A.x + B.x) / 2;
	temp.y = (A.y + B.y) / 2;
	return temp;
}

Point rectangle::Get_MidPoint(Point A, Point B, string name) {
	Point temp;
	temp.Name = name;
	temp.x = (A.x + B.x) / 2;
	temp.y = (A.y + B.y) / 2;
	return temp;
}

Point rectangle::Get_Center() {
	Point temp;
	float a_AC, b_AC, c_AC;
	float a_BD, b_BD, c_BD;

	a_AC = C.y - A.y;
	b_AC = A.x - C.x;
	c_AC = -A.x*C.y + C.x*A.y;

	a_BD = D.y - B.y;
	b_BD = B.x - D.x;
	c_BD = -B.x*D.y + D.x*B.y;


	temp.x = ((b_AC*c_BD) - (b_BD*c_AC)) / ((a_AC*b_BD) - (a_BD*b_AC));
	temp.y = ((a_BD*c_AC) - (a_AC*c_BD)) / ((a_AC*b_BD) - (a_BD*b_AC));

	return temp;
}

void rectangle::Draw_Rectangle(Point A, Point B, Point C, Point D, SDL_Renderer *ren) {
	Bresenham_Line(A.x, A.y, B.x, B.y, ren);
	Bresenham_Line(B.x, B.y, C.x, C.y, ren);
	Bresenham_Line(C.x, C.y, D.x, D.y, ren);
	Bresenham_Line(D.x, D.y, A.x, A.y, ren);
}

void rectangle::Draw_Projection(Point Angle, Point Edge1, Point Edge2, SDL_Renderer *ren) {
	Bresenham_Line(Edge1.x, Edge1.y, Edge2.x, Edge2.y, ren);

	float a_Edge = Edge2.y - Edge1.y;
	float b_Edge = Edge1.x - Edge2.x;
	float c_Edge = -Edge1.x*Edge2.y + Edge2.x*Edge1.y;


	float a_hei, b_hei, c_hei;
	Point temp;

	a_hei = -b_Edge;
	b_hei = a_Edge;
	c_hei = (b_Edge * Angle.x) - (a_Edge * Angle.y);

	temp.x = ((b_hei*c_Edge) - (b_Edge*c_hei)) / ((a_hei*b_Edge) - (a_Edge*b_hei));
	temp.y = ((a_Edge*c_hei) - (a_hei*c_Edge)) / ((a_hei*b_Edge) - (a_Edge*b_hei));
	Bresenham_Line(Angle.x, Angle.y, temp.x, temp.y, ren);
}

void rectangle::Draw_Circumscribed(SDL_Renderer *ren) {
	Point center;
	center = this->Get_Center();
	float r = sqrt((center.x - A.x)*(center.x - A.x) + (center.y - A.y)*(center.y - A.y));
	BresenhamDrawCircle(center.x, center.y, r, ren);
}