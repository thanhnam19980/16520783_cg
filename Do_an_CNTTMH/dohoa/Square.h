#pragma once
#include "Point.h"
#include "Line.h"
#include "Circle.h"

class Square {
public:
	Point A, B, C, D;
	float AB, BC, CD, DA;

	float a_AB, b_AB, c_AB;
	float a_BC, b_BC, c_BC;
	float a_CD, b_CD, c_CD;
	float a_DA, b_DA, c_DA;
public:
	Square();
	Point Get_MidPoint(Point A, Point B); //Lay trung diem
	Point Get_MidPoint(Point A, Point B, string name); //Lay trung diem co ten
	Point Get_Center(); //Lay giao diem hai duong cheo cua hinh vuong

	void Draw_Square(Point A, Point B, string name, SDL_Renderer *ren); //Ve h�nh vuong tu hai diem co san, tham so "name" l� ten cua hinh vuong can ve nhu ACDE 
	void Draw_Square(Point A, Point B, Point C, Point D, SDL_Renderer *ren); //Ve hinh vuong tu bon diem
	void Draw_InscribedCircle(SDL_Renderer *ren); //Ve duong tron noi tiep hinh vuong
	void Draw_Circumscribed(SDL_Renderer *ren); //Ve duong tron ngoai tiep hinh vuong
};


